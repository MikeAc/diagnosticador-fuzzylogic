#include "registroalumno.h"

registroAlumno::registroAlumno(QWidget *parent) :
    QDialog(parent)
{

       tab=new Tab();

    QFile File(":p/shopstyle.qss");
     File.open(QFile::ReadOnly);
     QString StyleShee = QLatin1String(File.readAll());
     File.close();

     this->setStyleSheet(StyleShee);
    this->setFixedHeight(280);
    movie=new QMovie(":/giphy.gif");
    connect(movie,SIGNAL(frameChanged(int)),this,SLOT(paintNewFrame(int)));
    movie->start();
    contWidget=new QListWidget(this);
    contWidget->setViewMode(QListView::IconMode);
    contWidget->setIconSize(QSize(100, 100));
    contWidget->setMovement(QListView::Static);
    contWidget->setMaximumWidth(128);
   contWidget->setSpacing(12);
   contWidget->setStyleSheet(" background-color:transparent; border:none; color:black;font: oblique bold 120% cursive");
   regist=new RegistroNinoWidg(this);
   pWidget=new QStackedWidget(this);
   getsNin=new getNino(tab);
   pWidget->addWidget(regist);
   createIcon();
   contWidget->setCurrentRow(0);
   QHBoxLayout *horizontalLayout = new QHBoxLayout;
      horizontalLayout->addWidget(contWidget);
      horizontalLayout->addWidget(pWidget, 1);


      QVBoxLayout *mainLayout = new QVBoxLayout;



       QGroupBox *group=new QGroupBox;
       group->setLayout(horizontalLayout);
       group->setObjectName("borders");
       pWidget->setCurrentIndex(contWidget->row(0));
       tab->addTab(group,tr("Registro"));
       tab->addTab(getsNin,trUtf8("Niños"));

        mainLayout->addWidget(tab);
        mainLayout->addStretch(1);
        mainLayout->addSpacing(12);

      setLayout(mainLayout);

      connect(regist->push,SIGNAL(pressed()),this,SLOT(accion()));

connect(getsNin->tableNino,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(accion(int,int)));

}

void registroAlumno::accion()
{
   if(regist->regist())
   {
    QWidget *wid=(QWidget *)parent();
    wid->show();
    this->hide();
    regist->limpiar();
    getsNin->loadNinos();

   }
}

void registroAlumno::accion(int w, int h)
{
    if(getsNin->addnew(w,h))
    {
    QWidget *wid=(QWidget *)parent();
    wid->show();
    this->hide();
    regist->limpiar();
    getsNin->mapped(getNino::ids);
    }
}

void registroAlumno::createIcon()
{
    QListWidgetItem *RegistrSec=new QListWidgetItem(QIcon(tr(":/lol.jpg")),tr("Alumno"),contWidget);
    RegistrSec->setTextAlignment(Qt::AlignCenter);
    RegistrSec->setFlags( Qt::ItemIsEnabled);

}

void registroAlumno::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

     QPixmap currentFrame = movie->currentPixmap();

     painter.drawPixmap(0,0,currentFrame.scaled(this->size()));
}

void registroAlumno::hideEvent(QHideEvent *)
{
    movie->stop();
}

void registroAlumno::showEvent(QShowEvent *)
{
    movie->start();
}

void registroAlumno::paintNewFrame(int)
{
    repaint();
}
