#include "fuzzyfer.h"

Fuzzyfer::Fuzzyfer()
{
    matrix[0][1].push_back(Cognitiva);
    matrix[0][2].push_back(Social);
    matrix[0][4].push_back(Social);
    matrix[0][4].push_back(Cognitiva);
    matrix[0][5].push_back(Social);
    matrix[0][5].push_back(Cognitiva);
    matrix[0][6].push_back(Social);
    matrix[0][6].push_back(Cognitiva);
    matrix[0][7].push_back(Social);
    matrix[0][8].push_back(Social);
    matrix[0][9].push_back(Cognitiva);
    matrix[0][10].push_back(Social);
    matrix[0][10].push_back(Cognitiva);
    matrix[0][11].push_back(Social);
    matrix[0][12].push_back(Cognitiva);
    matrix[0][13].push_back(Cognitiva);
    matrix[0][14].push_back(Cognitiva);
    matrix[0][15].push_back(Cognitiva);
    matrix[0][16].push_back(Cognitiva);
    matrix[0][17].push_back(Social);
    matrix[0][18].push_back(Social);
    matrix[0][19].push_back(Social);
    matrix[0][20].push_back(Social);
    matrix[0][21].push_back(Social);
    matrix[0][22].push_back(Social);
    matrix[0][23].push_back(Social);
    matrix[0][24].push_back(Social);
    matrix[0][25].push_back(Social);
    matrix[0][26].push_back(Social);
    matrix[0][27].push_back(Social);
    matrix[0][28].push_back(Social);
    matrix[0][28].push_back(Cognitiva);
    matrix[0][29].push_back(Social);
    matrix[0][30].push_back(Social);
    matrix[0][31].push_back(Cognitiva);
    matrix[0][32].push_back(Cognitiva);
    matrix[1][2].push_back(Social);
    matrix[1][2].push_back(Cognitiva);
    matrix[1][5].push_back(Social);
    matrix[1][5].push_back(Cognitiva);
    matrix[1][9].push_back(Cognitiva);
    matrix[1][10].push_back(Social);
    matrix[1][10].push_back(Cognitiva);
    matrix[1][11].push_back(Afectiva);
    matrix[1][12].push_back(Cognitiva);
    matrix[1][14].push_back(Cognitiva);
    matrix[1][15].push_back(Cognitiva);
    matrix[1][16].push_back(Cognitiva);
    matrix[1][25].push_back(Cognitiva);
    matrix[1][26].push_back(Cognitiva);
    matrix[1][27].push_back(Cognitiva);
    matrix[1][28].push_back(Social);
    matrix[1][28].push_back(Cognitiva);
    matrix[1][31].push_back(Cognitiva);
   matrix[1][32].push_back(Cognitiva);
   matrix[2][0].push_back(Social);
   matrix[2][1].push_back(Social);
   matrix[2][1].push_back(Cognitiva);
   matrix[2][3].push_back(Social);
   matrix[2][4].push_back(Social);
   matrix[2][6].push_back(Social);
   matrix[2][6].push_back(Cognitiva);
   matrix[2][7].push_back(Social);
   matrix[2][8].push_back(Social);
   matrix[2][11].push_back(Social);
   matrix[2][12].push_back(Social);
   matrix[2][12].push_back(Cognitiva);
   for (int i = 17; i <= 24; ++i) {
       matrix[2][i].push_back(Social);
   }
   matrix[2][29].push_back(Social);
   matrix[2][30].push_back(Social);

   for (int i = 0; i < 11; ++i) {
       if(i!=3 || i!=4)
       matrix[3][i].push_back(Social);
   }
    matrix[3][9].push_back(Cognitiva);
    matrix[3][10].push_back(Cognitiva);
    matrix[3][12].push_back(Social);
    matrix[3][12].push_back(Cognitiva);

    for (int i = 13; i < 17; ++i) {
         matrix[3][i].push_back(Cognitiva);
    }
     matrix[3][17].push_back(Social);
      matrix[3][18].push_back(Social);
       matrix[3][23].push_back(Social);
       for (int i = 25; i < 29; ++i) {
            matrix[3][i].push_back(Social);
             matrix[3][i].push_back(Cognitiva);
       }
    matrix[3][31].push_back(Social);
    matrix[3][31].push_back(Cognitiva);

    matrix[4][0].push_back(Social);
    matrix[4][1].push_back(Social);
    matrix[4][2].push_back(Afectiva);
    matrix[4][2].push_back(Cognitiva);
    matrix[4][2].push_back(Social);

    for (int i = 4; i < 8; ++i) {
        matrix[4][i].push_back(Social);

    }
    matrix[4][9].push_back(Social);
    matrix[4][9].push_back(Cognitiva);
    matrix[4][10].push_back(Social);
    matrix[4][10].push_back(Cognitiva);

    for (int i = 12; i < 17; ++i) {
        matrix[4][i].push_back(Social);
        matrix[4][i].push_back(Cognitiva);
    }
    for (int i = 17; i < 34; ++i) {
        if(i!=28)
        matrix[4][i].push_back(Social);
    }

    matrix[4][29].push_back(Cognitiva);
    matrix[4][31].push_back(Cognitiva);
    matrix[4][32].push_back(Cognitiva);
    matrix[4][33].push_back(Social);

    matrix[5][0].push_back(Social);
    matrix[5][2].push_back(Social);
    matrix[5][3].push_back(Cognitiva);
    matrix[5][3].push_back(Social);
    for (int i = 4; i < 9; ++i) {
        matrix[5][i].push_back(Social);
    }

    matrix[5][6].push_back(Afectiva);
    matrix[5][7].push_back(Afectiva);
    matrix[5][9].push_back(Cognitiva);
    for (int i = 10; i < 15; ++i) {
        matrix[5][i].push_back(Social);
        matrix[5][i].push_back(Cognitiva);
    }
    matrix[5][15].push_back(Cognitiva);
    matrix[5][16].push_back(Cognitiva);
    matrix[5][15].push_back(Cognitiva);

    for (int i = 17; i < 30; ++i) {
        matrix[5][i].push_back(Social);
    }
    matrix[5][25].push_back(Cognitiva);
    matrix[5][26].push_back(Cognitiva);
    matrix[5][27].push_back(Cognitiva);
    matrix[5][28].push_back(Cognitiva);
    matrix[5][29].push_back(Cognitiva);

    matrix[5][31].push_back(Social);
    matrix[5][32].push_back(Social);
    matrix[5][33].push_back(Social);

    matrix[5][31].push_back(Cognitiva);
    matrix[5][32].push_back(Cognitiva);
    matrix[5][33].push_back(Cognitiva);

    matrix[6][0].push_back(Social);
    matrix[6][0].push_back(Cognitiva);
    matrix[6][1].push_back(Social);
    matrix[6][2].push_back(Social);
    matrix[6][2].push_back(Cognitiva);

    matrix[6][3].push_back(Cognitiva);
    for (int i = 4; i < 12; ++i) {
        matrix[6][i].push_back(Social);
        matrix[6][i].push_back(Cognitiva);
        }
    matrix[6][12].push_back(Cognitiva);
    matrix[6][13].push_back(Cognitiva);
    matrix[6][13].push_back(Afectiva);
    matrix[6][14].push_back(Cognitiva);
    matrix[6][15].push_back(Cognitiva);
    matrix[6][16].push_back(Social);
    matrix[6][16].push_back(Cognitiva);
    for (int i = 17; i < 25; ++i) {
        matrix[6][i].push_back(Social);
        matrix[6][i].push_back(Afectiva);

    }
    for (int i = 25; i < 34; ++i) {
        matrix[6][i].push_back(Cognitiva);
    }
    matrix[6][31].push_back(Social);
    matrix[7][2].push_back(Social);
    matrix[7][7].push_back(Social);
    matrix[7][8].push_back(Social);
    matrix[7][9].push_back(Social);
    matrix[7][11].push_back(Social);

    for (int i = 17; i < 25; ++i) {
        matrix[7][i].push_back(Social);
        matrix[7][i].push_back(Afectiva);
    }

    for (int i = 25; i < 28; ++i) {
        matrix[7][i].push_back(Social);
        matrix[7][i].push_back(Cognitiva);
    }
    matrix[7][28].push_back(Social);
    matrix[7][30].push_back(Social);

    matrix[7][32].push_back(Social);
    matrix[7][32].push_back(Cognitiva);
    matrix[7][33].push_back(Cognitiva);

    matrix[8][0].push_back(Social);

    matrix[8][1].push_back(Social);
    matrix[8][1].push_back(Afectiva);
    matrix[8][1].push_back(Cognitiva);

    matrix[8][2].push_back(Social);
    matrix[8][4].push_back(Social);
    matrix[8][5].push_back(Social);
    matrix[8][5].push_back(Cognitiva);
    matrix[8][7].push_back(Social);
    matrix[8][8].push_back(Social);
    for (int i = 10; i < 14; ++i) {
        matrix[8][i].push_back(Social);
    }
    matrix[8][10].push_back(Cognitiva);
    matrix[8][11].push_back(Cognitiva);
    matrix[8][13].push_back(Afectiva);

    for (int i = 17; i < 25; ++i) {
        matrix[8][i].push_back(Social);
        matrix[8][i].push_back(Afectiva);
    }
    matrix[8][30].push_back(Social);
    matrix[8][30].push_back(Afectiva);

    matrix[9][4].push_back(Social);
    matrix[9][5].push_back(Social);
    matrix[9][5].push_back(Cognitiva);

    for (int i = 10; i < 14; ++i) {
     matrix[9][i].push_back(Social);
    }

    matrix[9][10].push_back(Afectiva);
    matrix[9][11].push_back(Cognitiva);

    matrix[9][12].push_back(Afectiva);
    matrix[9][12].push_back(Cognitiva);
    matrix[9][13].push_back(Afectiva);
    for (int i = 17; i < 25; ++i) {
        matrix[9][i].push_back(Social);
        matrix[9][i].push_back(Afectiva);
    }

    matrix[9][30].push_back(Social);
    matrix[9][30].push_back(Afectiva);

    matrix[10][0].push_back(Afectiva);
    matrix[10][2].push_back(Social);

    for (int i = 4; i < 7; ++i) {
        matrix[10][i].push_back(Social);
    }

    matrix[10][4].push_back(Afectiva);
    matrix[10][5].push_back(Cognitiva);
    matrix[10][6].push_back(Afectiva);
    matrix[10][10].push_back(Social);
    matrix[10][10].push_back(Afectiva);

    matrix[10][12].push_back(Afectiva);
    matrix[10][12].push_back(Cognitiva);
    matrix[10][12].push_back(Social);
    matrix[10][13].push_back(Social);
    matrix[10][13].push_back(Afectiva);

    for (int i = 17; i < 25; ++i) {
        matrix[10][i].push_back(Social);
        matrix[10][i].push_back(Afectiva);
    }

    matrix[10][30].push_back(Social);
    matrix[10][30].push_back(Afectiva);

    matrix[11][0].push_back(Afectiva);

    for (int i = 3; i < 7; ++i) {
        matrix[11][i].push_back(Social);

    }

    matrix[11][3].push_back(Afectiva);
    matrix[11][4].push_back(Afectiva);
    matrix[11][5].push_back(Cognitiva);
    matrix[11][6].push_back(Afectiva);

    matrix[11][10].push_back(Social);
    matrix[11][10].push_back(Afectiva);
    matrix[11][12].push_back(Afectiva);
    matrix[11][12].push_back(Social);
    matrix[11][12].push_back(Cognitiva);

    matrix[11][13].push_back(Social);
    matrix[11][13].push_back(Afectiva);

    for (int i = 17; i <25; ++i) {
        matrix[11][i].push_back(Social);
        matrix[11][i].push_back(Afectiva);
    }
    matrix[11][28].push_back(Social);
    matrix[11][28].push_back(Afectiva);
    matrix[11][30].push_back(Social);
    matrix[11][30].push_back(Afectiva);

    matrix[12][2].push_back(Social);

    for (int i = 3; i < 7; ++i) {
         matrix[12][i].push_back(Social);
    }
     matrix[12][3].push_back(Afectiva);
     matrix[12][4].push_back(Afectiva);
     matrix[12][5].push_back(Cognitiva);
     matrix[12][6].push_back(Afectiva);
     matrix[12][10].push_back(Afectiva);
     matrix[12][10].push_back(Social);

      matrix[12][12].push_back(Afectiva);
       matrix[12][12].push_back(Cognitiva);
        matrix[12][12].push_back(Social);

        for (int i = 17; i < 25; ++i) {
             matrix[12][i].push_back(Social);
              matrix[12][i].push_back(Afectiva);

        }
       matrix[12][30].push_back(Social);
       matrix[12][30].push_back(Afectiva);

        matrix[13][0].push_back(Afectiva);
        matrix[13][1].push_back(Afectiva);
        matrix[13][2].push_back(Social);
        matrix[13][3].push_back(Afectiva);
        matrix[13][3].push_back(Social);
        matrix[13][4].push_back(Afectiva);
        matrix[13][4].push_back(Social);
        matrix[13][5].push_back(Social);
        matrix[13][5].push_back(Cognitiva);

        matrix[13][7].push_back(Social);
        matrix[13][8].push_back(Social);
        matrix[13][10].push_back(Social);
        matrix[13][10].push_back(Afectiva);

        matrix[13][12].push_back(Social);
        matrix[13][12].push_back(Afectiva);
        matrix[13][12].push_back(Cognitiva);

        for (int i = 17; i < 25; ++i) {
            matrix[13][i].push_back(Social);
            matrix[13][i].push_back(Afectiva);
        }
        matrix[13][30].push_back(Social);
        matrix[13][30].push_back(Afectiva);

        matrix[13][33].push_back(Social);
        matrix[13][33].push_back(Cognitiva);
        createSentencias();


}

QList<QVariant> Fuzzyfer::start()
{
    QList< QList<QVariant> > list,list2;
QList<QVariant> *last;

QList<QVariant> listd;
double cog=0,soci=0,afect=0,hiperactividad=0,atencion=0;
int contcog=0,contsoci=0,contafect=0,conthiper=0,contatent=0;
int control;
    for (int i = 0; i <PreguntaItem::activadasPreguntas.size(); i++) {
        for (int j = 0; j < PreguntaItem::activadasProblemas.size(); j++) {
           
            if(!matrix[PreguntaItem::activadasPreguntas.at(i)->getPosNum()][PreguntaItem::activadasProblemas.at(j)->getPosNum()].isEmpty())
            {

                for (int l = 0; l < matrix[PreguntaItem::activadasPreguntas[i]->getPosNum()][PreguntaItem::activadasProblemas[j]->getPosNum()].size(); ++l) {
                    last=new QList<QVariant>();

/*PreguntaItem::activadasPreguntas.at(i)->getMyvalue(),PreguntaItem::activadasProblemas.at(j)->getMyvalue())*/
                    last->push_back(calculate(returnTheRules(PreguntaItem::activadasPreguntas.at(i)->getPosNum(),PreguntaItem::activadasProblemas.at(j)->getMitype()),PreguntaItem::activadasPreguntas.at(i)->getPosNum(),PreguntaItem::activadasProblemas.at(j)->getMitype(),PreguntaItem::activadasPreguntas.at(i)->getMyvalue(),PreguntaItem::activadasProblemas.at(j)->getMyvalue()));

                    last->push_back(rules(matrix[PreguntaItem::activadasPreguntas.at(i)->getPosNum()][PreguntaItem::activadasProblemas.at(j)->getPosNum()].at(l)));
                    list.push_back(*last);

                }
            }

            
        }

    }
    
    

   for (int i = 0; i < PreguntaItem::activadasPreguntas.size(); ++i) {

      hiperactividad+=calculateone(returnTheRules(PreguntaItem::activadasPreguntas.at(i)->getPosNum()),PreguntaItem::activadasPreguntas.at(i)->getPosNum(),PreguntaItem::activadasPreguntas.at(i)->getMyvalue());

      conthiper++;
}


   for (int i = 0; i < PreguntaItem::activadasProblemas.size(); ++i) {

control=PreguntaItem::activadasProblemas.at(i)->getMitype();
       switch (control)
       {
       case TranstornoSocial:

           soci+=calculateone(returnTheRules(PreguntaItem::activadasProblemas.at(i)->getPosNum()),PreguntaItem::activadasProblemas.at(i)->getPosNum(),PreguntaItem::activadasProblemas.at(i)->getMyvalue());
           contsoci++;

           break;
       case DeficitAtencion:

           atencion+=calculateone(returnTheRules(PreguntaItem::activadasProblemas.at(i)->getPosNum()),PreguntaItem::activadasProblemas.at(i)->getPosNum(),PreguntaItem::activadasProblemas.at(i)->getMyvalue());
           contatent++;
           break;
       case Hiperactividad:
           hiperactividad+=calculateone(returnTheRules(PreguntaItem::activadasProblemas.at(i)->getPosNum()),PreguntaItem::activadasProblemas.at(i)->getPosNum(),PreguntaItem::activadasProblemas.at(i)->getMyvalue());
           conthiper++;
           break;
      default:
           qDebug()<<"Esto no deberia pasar";
       }

   }



    for (int i = 0; i < list.size(); ++i) {
        if(list.at(i).at(1).toString().compare("social")==0)
        {

            if(list.at(i).at(0).toDouble()>=0)
            {
            contsoci++;

           // qDebug()<<list.at(i).at(0).toDouble();
            soci+=list.at(i).at(0).toDouble();
}

        }
        else
        {
            if(list.at(i).at(1).toString().compare("Cognitiva")==0)
           {
                if(list.at(i).at(0).toDouble()>=0)
                {

                contcog++;
              cog+=list.at(i).at(0).toDouble();
}

            }
            else
            {

                if(list.at(i).at(0).toDouble()>=0)
                {
              afect+=list.at(i).at(0).toDouble();

                contafect++;

}
    }
}
    }
 if(conthiper!=0)
    hiperactividad=hiperactividad/conthiper;

 if (contatent!=0)
    atencion=atencion/contatent;

 if(contsoci!=0)
   soci=soci/contsoci;

 if(contcog!=0)
   cog=cog/contcog;

 if(contafect!=0)
   afect=afect/contafect;



  listd.push_back(soci);
  listd.push_back(cog);
   listd.push_back(afect);
   listd.push_back(hiperactividad);
    listd.push_back(atencion);






    return listd;
}



QStringList Fuzzyfer::returnTheRules(int Pregunta, int Problema)
{

 //qDebug()<<sentencias.filter(QRegExp(rules(Pregunta)+" .*"+rules(Problema)))<<" "<<rules(Pregunta)+" .*"+rules(Problema);
    return sentencias.filter(QRegExp(rules(Pregunta)+" .*"+rules(Problema)));
    
}

QString Fuzzyfer::rules(int sentencia)
{

    switch (sentencia) {
    case Hiperactividad:

        return QString("hiperactividad");
        break;
    case HiperactivadAtencion:
        return QString("AtenHiper");
        break;
    case TranstornoSocial:
        return QString("social");
        break;
    case DeficitAtencion:
        return QString("atencion");

    case Social:

        return QString("social");
        break;
    case Cognitiva:
        return QString("Cognitiva");
        break;
    case Afectiva:
        return QString("Afectiva");
        break;

        default:
        return QString("pregunta"+QString().setNum(sentencia));
        break;
    }
}

fl::scalar Fuzzyfer::calculateone(QStringList list,int pregunta,double valotPregunta)
{
    fl::Engine *engine = new fl::Engine("One");

       fl::InputVariable *one = new fl::InputVariable;

       one->setName((QString("pregunta")+QString().setNum(pregunta)).toStdString());
       one->setRange(0.000, 100.000);
       one->addTerm(new fl::Triangle("normal", 0.000,40.00));
       one->addTerm(new fl::Triangle("low", 20.00, 60.000));
       one->addTerm(new fl::Triangle("med", 40.000,80.000 ));
       one->addTerm(new fl::Triangle("high", 60.000, 100.000));

       engine->addInputVariable(one);

       fl::OutputVariable* out = new fl::OutputVariable;
       out->setName("out");
       out->setRange(0.000, 100.000);
       out->setDefaultValue(0);
       out->addTerm(new fl::Triangle("normal", 0.000, 40.000));
       out->addTerm(new fl::Triangle("low", 20.000, 60.000));
       out->addTerm(new fl::Triangle("med", 40.000, 80.000));
       out->addTerm(new fl::Triangle("high",60.000,100.000));
       engine->addOutputVariable(out);

       fl::RuleBlock* ruleblock = new fl::RuleBlock;
       for (int i = 0; i < list.size(); ++i) {

           ruleblock->addRule(fl::Rule::parse(list.at(i).toStdString(),engine));
       }

       engine->addRuleBlock(ruleblock);

       engine->configure("Minimum", "Maximum", "AlgebraicProduct", "AlgebraicSum", "Centroid");

       std::string status;
       if (! engine->isReady(&status))
           throw fl::Exception("Engine not ready. "
               "The following errors were encountered:\n" + status, FL_AT);


           fl::scalar value=valotPregunta;
           one->setInputValue(value);
           engine->process();

           return out->getOutputValue();

}

fl::scalar Fuzzyfer::calculate(QStringList sent,int PreguntaType,int ProblemaType,double valorPregunta,double ValorProblema)
{
    fl::Engine* engine = new fl::Engine("ProblemasTodos");

    fl::InputVariable *Pregunta = new fl::InputVariable;
    Pregunta->setName(rules(PreguntaType).toStdString());

    Pregunta->setRange(0.000, 100.000);
    Pregunta->addTerm(new fl::Triangle("normal", 0.000,40.000));
    Pregunta->addTerm(new fl::Triangle("low",20.000,60.000));
    Pregunta->addTerm(new fl::Triangle("med",40.00, 80.000));
    Pregunta->addTerm(new fl::Triangle("high",60.000,100.000));
    engine->addInputVariable(Pregunta);

    fl::InputVariable *Problema = new fl::InputVariable;

    Problema->setName(rules(ProblemaType).toStdString());

    Problema->setRange(0.000, 100.000);
    Problema->addTerm(new fl::Triangle("normal", 0.000,40.000));
    Problema->addTerm(new fl::Triangle("low",20.000,60.000));
    Problema->addTerm(new fl::Triangle("med",40.00, 80.000));
    Problema->addTerm(new fl::Triangle("high",60.000,100.000));
    engine->addInputVariable(Problema);

    fl::OutputVariable *res = new fl::OutputVariable;
    res->setName("Res");
    res->setRange(0.000,100.000);
    res->setDefaultValue(-1);
    res->addTerm(new fl::Triangle("normal", 0.000,40.000));
    res->addTerm(new fl::Triangle("low",20.000,60.000));
    res->addTerm(new fl::Triangle("med",40.00, 60.000));
    res->addTerm(new fl::Triangle("high",80.000,100.000));
    engine->addOutputVariable(res);

    fl::RuleBlock* ruleblock = new fl::RuleBlock;
    sent.removeDuplicates();
    for (int i = 0; i < sent.size(); ++i) {


         ruleblock->addRule(fl::Rule::parse(sent.at(i).toStdString(), engine));
    }


    engine->addRuleBlock(ruleblock);

    //conjuction
    engine->configure("Minimum", "Maximum", "AlgebraicProduct", "AlgebraicSum", "Centroid");


    std::string status;
    if (! engine->isReady(&status))
         throw fl::Exception("Engine not ready. "
               "The following errors were encountered:\n" + status, FL_AT);

       fl::scalar preg=valorPregunta;
       fl::scalar probl=ValorProblema;

        Pregunta->setInputValue(preg);
        Problema->setInputValue(probl);
        engine->process();
        return res->getOutputValue();


           
}

QStringList Fuzzyfer::returnTheRules(int Pregunta)
{

   return sentenciasOne.filter(QRegExp(rules(Pregunta)+" .*"));
}

void Fuzzyfer::createSentencias()
{


    for (int i = 0; i < 14; ++i) {
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and hiperactividad is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and hiperactividad is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and hiperactividad is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and hiperactividad is normal then Res is low");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and hiperactividad is low then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and hiperactividad is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and hiperactividad is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and hiperactividad is low then Res is med");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and hiperactividad is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and hiperactividad is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and hiperactividad is med then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and hiperactividad is med then Res is high");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and hiperactividad is high then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and hiperactividad is high then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and hiperactividad is high then Res is high");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and hiperactividad is high then Res is high");
    }

    //*********************************************************************************************

    for (int i = 0; i < 14; ++i) {
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and AtenHiper is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and AtenHiper is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and AtenHiper is normal then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and AtenHiper is normal then Res is med");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and  AtenHiper is low then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and AtenHiper is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and AtenHiper is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and AtenHiper is low then Res is med");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and AtenHiper is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and AtenHiper is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and AtenHiper is med then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and AtenHiper is med then Res is high");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and AtenHiper is high then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and AtenHiper is high then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and AtenHiper is high then Res is high");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and AtenHiper is high then Res is high");
    }
    //******************************************************************************************

    for (int i = 0; i < 14; ++i) {
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and social is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and social is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and social is normal then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and social is normal then Res is low");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and social is low then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and social is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and social is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and social is low then Res is med");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and social is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and social is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and social is med then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and social is med then Res is high");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and social is high then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and social is high then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and social is high then Res is high");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and social is high then Res is high");
    }

    //********************************************************************************************

    for (int i = 0; i < 14; ++i) {
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and atencion is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and atencion is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and atencion is normal then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and atencion is normal then Res is low");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and atencion is low then Res is normal");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and atencion is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and atencion is low then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and atencion is low then Res is med");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and atencion is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and atencion is med then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and atencion is med then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and atencion is med then Res is high");

        sentencias.push_back("if pregunta"+QString().setNum(i)+" is normal and atencion is high then Res is low");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is low and atencion is high then Res is med");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is med and atencion is high then Res is high");
        sentencias.push_back("if pregunta"+QString().setNum(i)+" is high and atencion is high then Res is high");
    }
    for (int i = 0; i <14 ; ++i) {
        
        sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is normal then out is normal ");
         sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is low then out is  low");
          sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is med then out is med");
           sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is high then out is high");
    
    }
    
    for (int i = 14; i <23; ++i) {
        
    
    sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is normal then out is normal ");
     sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is low then out is  low");
      sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is med then out is med");
       sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is high then out is high");

    }

    for (int i = 23; i <31; ++i) {


    sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is normal then out is normal ");
     sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is low then out is  low");
      sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is med then out is med");
       sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is high then out is high");

    }
    for (int i = 31; i <39; ++i) {


    sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is normal then out is normal ");
     sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is low then out is  low");
      sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is med then out is med");
       sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is high then out is high");

    }
    for (int i = 39; i <48; ++i) {


    sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is normal then out is normal ");
     sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is low then out is  low");
      sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is med then out is med");
       sentenciasOne.push_back("if pregunta"+QString().setNum(i)+" is high then out is high");

    }



}



PruebaFuzz::PruebaFuzz()
{

   }


