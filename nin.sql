-- Creator:       MySQL Workbench 6.1.7/ExportSQLite plugin 2009.12.02
-- Author:        Mike
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2014-11-20 01:32
-- Created:       2014-11-20 01:12
PRAGMA foreign_keys = OFF;

-- Schema: nino
BEGIN;
CREATE TABLE "Nino"(
  "idNino" INTEGER PRIMARY KEY NOT NULL,
  "Nombre" VARCHAR(40) NOT NULL,
  "Edad" INTEGER NOT NULL,
  "Escolaridad" VARCHAR(16) NOT NULL,
  "grado" INTEGER,
   "fecha" DATE not null
 
);
CREATE TABLE "Resultados"(
  "idres" INTEGER PRIMARY KEY NOT NULL,
  "hiper" DOUBLE,
  "atenc" DOUBLE,
  "soc" DOUBLE,
  "cog" DOUBLE,
  "afect" DOUBLE,
  "fecha" DATE,
  "fk_idNino" INTEGER NOT NULL,
  CONSTRAINT "fk_Resultados_Nino"
    FOREIGN KEY("fk_idNino")
    REFERENCES "Nino"("idNino")
);
CREATE INDEX "Resultados.fk_Resultados_Nino_idx" ON "Resultados"("fk_idNino");
COMMIT;
