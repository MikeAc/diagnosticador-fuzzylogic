#ifndef DIALOGWM_H
#define DIALOGWM_H

#include <QDialog>
#include<QPainter>
#include<QTimer>
#include<QMovie>
class dialogWM : public QDialog
{
    Q_OBJECT
public:
    explicit dialogWM(QWidget *parent = 0);
    void destroyme();
protected:

    void paintEvent(QPaintEvent *);

private:
QMovie *movie;

signals:
private slots:
 void paintNewFrame(int);
};

#endif // DIALOGWM_H
