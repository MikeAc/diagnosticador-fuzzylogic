#ifndef FUZZYFER_H
#define FUZZYFER_H
#include"widgets.h"
#include<iostream>
#include<fuzzyfer.h>
#include"fl/Headers.h"

class PruebaFuzz
{
    PruebaFuzz();
};

class Fuzzyfer
{
public:
    Fuzzyfer();
    QList<QVariant> start();

QList<int> matrix[14][34];
   private:

    QStringList returnTheRules(int Pregunta, int Problema);
    QString rules(int sentencia);
    fl::scalar calculateone(QStringList, int pregunta, double valotPregunta);
    fl::scalar calculate(QStringList sent, int PreguntaType, int ProblemaType, double valorPregunta, double ValorProblema);
   QStringList returnTheRules(int Pregunta);

   

    void createSentencias();
    QStringList sentencias;
    QStringList sentenciasOne;
};

#endif // FUZZYFER_H
