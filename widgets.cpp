#include "widgets.h"
#include<QDebug>
QList<PreguntaItem*> PreguntaItem::activadasPreguntas;
QList<PreguntaItem*> PreguntaItem::activadasProblemas;
int getNino::ids;
RegistroNinoWidg::RegistroNinoWidg(QWidget *parent) :
    QWidget(parent)
{

    QFile File(":p/shopstyle.qss");
     File.open(QFile::ReadOnly);
     QString StyleShee = QLatin1String(File.readAll());
     File.close();
     this->setStyleSheet(tr(" QLabel {color:white;font: oblique bold 120% cursive; font-size:12pt;} QGroupBox{ color:black; font-family:Luxi Sans; font-size:24pt; }").append(StyleShee));
   Conexion::loaddb();


    mainLayout=new QVBoxLayout(this);
    lineNombreAlumno=new QLineEdit(this);
    LabelAlumno=new QLabel(tr("Nombre del Alumno(a)"),this);
    LabelEdad=new QLabel(tr("Fecha de Nacimiento:"),this);
    LabelEscuela=new QLabel(tr("Escolaridad del Alumno(a)"),this);
    LabelGrado=new QLabel(tr("Grado  en curso del Alumno(a)"),this);
    LabelSexo=new QLabel(tr("Sexo del Alumno(a)"),this);
    Labeldia=new QLabel(tr("Dia: "),this);
    Labelmes=new QLabel(tr("Mes: "),this);
    Labelanio=new QLabel(trUtf8("Año: "),this);

    push=new QPushButton(tr("Seguir"));
QRegExpValidator *vl=new  QRegExpValidator(QRegExp("^[A-z ]{3,40}$"));
    push->setFixedSize(push->sizeHint());


    lineNombreAlumno->setPlaceholderText("Ej:Julieta Ramirez Tabarez");
  lineNombreAlumno->setMinimumWidth(180);
    dia=new QSpinBox(this);
    mes=new QSpinBox(this);
    anio=new QSpinBox(this);

    Grado=new QComboBox(this);

    Escuela=new QComboBox(this);
    Sexo=new QComboBox(this);

    grid=new QGridLayout(this);

    box=new QGroupBox(tr("Registro del Alumno"),this);


    Escuela->setFixedSize(115,23);
    Escuela->addItem( QIcon(tr(":/kinder.png")),tr("Kinder"));
    Escuela->addItem( QIcon(tr(":/primaria.png")),tr("Primaria"));
    Escuela->addItem( QIcon(tr(":/secundaria.png")),tr("Secundaria"));
    Escuela->addItem( QIcon(tr(":/Preparatoria.png")),tr("Preparatoria"));
    Escuela->addItem( QIcon(tr(":/universidad.png")),tr("Universidad"));

    dia->setRange(1,31);
    mes->setRange(1,12);
    anio->setRange(1900,2200);
    dia->setFixedSize(dia->sizeHint());
    mes->setFixedSize(mes->sizeHint());
    anio->setFixedSize(anio->sizeHint());

    Sexo->addItem(QIcon(tr(":/masculino")),tr("Masculino"));
    Sexo->addItem(QIcon(tr(":/femenino")),tr("Femenino"));
    Sexo->setFixedSize(Sexo->sizeHint());

 QHBoxLayout *hbox=new QHBoxLayout(this);
 hbox->addWidget(Labeldia);
 hbox->addWidget(dia);
 hbox->addWidget(Labelmes);
 hbox->addWidget(mes);
 hbox->addWidget(Labelanio);
 hbox->addWidget(anio);
 hbox->addStretch(0);
 anio->setValue(QDate::currentDate().year());
    grid->addWidget(LabelAlumno,0,0);
    grid->addWidget(lineNombreAlumno,0,1);
    grid->addWidget(LabelEdad,1,0);
  grid->addLayout(hbox,1,1);
    grid->addWidget(LabelSexo,2,0);
    grid->addWidget(Sexo,2,1);
    grid->addWidget(LabelEscuela,3,0);
    grid->addWidget(Escuela,3,1);
    grid->addWidget(LabelGrado,4,0);
    grid->addWidget(Grado,4,1);
    grid->addWidget(push,5,2);

    listkind<<"1"<<"2"<<"3";
    listPrim<<"1"<<"2"<<"3"<<"4"<<"5"<<"6";
    listSec<<"1"<<"2"<<"3";
    listPrep<<trUtf8("1")<<trUtf8("2")<<trUtf8("3");
    listUniv<<"1";



    Grado->addItems(listkind);
    box->setLayout(grid);
    mainLayout->addWidget(box);
    this->setMinimumSize(455,188);
    this->setLayout(mainLayout);
    Grado->setFixedSize(77,23);

    connect(Escuela,SIGNAL(currentIndexChanged(int)),this,SLOT(cambMenu(int)));
connect(lineNombreAlumno,SIGNAL(textChanged(QString)),this,SLOT(ups(QString)));
lineNombreAlumno->setValidator(vl);

}

void RegistroNinoWidg::limpiar()
{
    lineNombreAlumno->clear();
}


bool RegistroNinoWidg::regist()
{
  QString nom= lineNombreAlumno->text(),dias=dia->text(),mess=mes->text();
  if(nom.isEmpty())
  {
      QMessageBox::critical(this,tr("Error"),tr("Le falta campos que llenar"));
      return false;
  }
  while (nom.at(0)==' ') {
     nom.remove(0,1);
  }
Conexion::query.exec("select count(*) from Nino where Nombre='"+nom+"'");
Conexion::query.next();
qDebug()<<"select count(*) from Nino where Nombre="+nom+"";
qDebug()<<Conexion::query.value(0).toInt();
if(Conexion::query.value(0).toInt()>0)
{
    QMessageBox::critical(this,tr("Error"),trUtf8("El niño ya fue sido registrado"));
    return false;
}

if(dia->value()<10)
{dias="0"+dias;

  }
if(mes->value()<10)
{mess="0"+mess;

  }

 Conexion::query.exec("insert into Nino(idNino,Nombre,Edad,Escolaridad,grado,fecha)  values ((select max(idNino)+1 from Nino),'"+nom+"',cast(((julianday('now')-julianday('"+anio->text()+"-"+mess+"-"+dias+"'))/365)+.00001 as Integer),'"+Escuela->currentText()+"',"+Grado->currentText()+",'"+anio->text()+"-"+mess+"-"+dias+"')");
 Conexion::query.exec("select idNino from Nino where Nombre='"+nom+"'");
 Conexion::query.next();
 getNino::ids=Conexion::query.value(0).toInt();
 lineNombreAlumno->clear();
 dia->setValue(1);
 mes->setValue(1);
 anio->setValue(1);
 Escuela->setCurrentIndex(0);
 Grado->setCurrentIndex(0);
 return true;
}



void RegistroNinoWidg::cambMenu(int index)
{
    qDebug()<<index;
    switch(index)
    {
    case 0:
        Grado->clear();
        Grado->addItems(listkind);

        break;
    case 1:
        Grado->clear();
        Grado->addItems(listPrim);

        break;
     case 2:
        Grado->clear();
        Grado->addItems(listSec);

        break;
     case 3:
        Grado->clear();
        Grado->addItems(listPrep);

        break;
     case 4:
        Grado->clear();
        Grado->addItems(listUniv);

        break;
    }
}

void RegistroNinoWidg::ups(QString str)
{

   lineNombreAlumno->setText(str.toUpper());
}


PreguntaItem::PreguntaItem(QString Pregunta,int numpregunta,int var,QWidget *parent):
    QWidget(parent)
{

    QFile File("qrc:p/widgets.qss");
     File.open(QFile::ReadOnly);
     QString StyleShee = QLatin1String(File.readAll());
     File.close();

 this->setStyleSheet(StyleShee);
    PreguntaPrincipal=new QLabel("<p><br /><span style=\"font-size:18px;\">"+Pregunta+"</span></p>");

    dial=new QSlider(Qt::Horizontal,this);
    mainlayout=new QGridLayout(this);
    labelporcent=new QLabel("Porcentaje:");

    siRB=new QRadioButton("si",this);
    noRB=new QRadioButton("no",this);

    prgres=new QProgressBar(this);
    QLabel *label=new QLabel("                                   ");

    mainlayout->addWidget(PreguntaPrincipal,0,0);
    mainlayout->addWidget(noRB,1,0);
    mainlayout->addWidget(siRB,2,0);
    mainlayout->addWidget(labelporcent,3,0);
    mainlayout->addWidget(label,3,1);
    mainlayout->addWidget(dial,4,0);
    mainlayout->addWidget(prgres,5,0);


    PreguntaPrincipal->setObjectName("Titulo");
    labelporcent->setObjectName("Titulo");


    dial->setStyleSheet("background-color:rgb(230,245,225,150)");
    QPalette pal(QColor(230,245,225,150));
    pal.setBrush(QPalette::Window,QBrush(QColor(230,245,225)));

    dial->setPalette(pal);
   setLayout(mainlayout);

   noRB->setChecked(true);
   connect(noRB,SIGNAL(toggled(bool)),this,SLOT(disactivateRB()));
   connect(siRB,SIGNAL(toggled(bool)),this,SLOT(activateRB()));
   dial->setDisabled(true);
   //porcent->setDisabled(true);
   labelporcent->setDisabled(true);
   //porcent->setMaximumSize(porcent->sizeHint());

   minum=numpregunta;
   mitype=var;
   cont=0;
prgres->setRange(0,100);
dial->setRange(0,99);
dial->setValue(0);
dial->setMaximumWidth(this->width()/2);
 prgres->setMaximumWidth(this->width()/2);
   connect(dial,SIGNAL(valueChanged(int)),this,SLOT(changue(int)));
}

int PreguntaItem::getMyvalue()
{
 return dial->value();
}

int PreguntaItem::getPosNum()
{   if(minum<14)
    return minum;
    else
        return minum-14;
}
int PreguntaItem::getMitype() const
{
    return mitype;
}

void PreguntaItem::hideEvent(QHideEvent *)
{

    dial->setDisabled(true);
    noRB->setChecked(true);
    if(minum<14){
    if(!activadasPreguntas.isEmpty())
   activadasPreguntas.clear();
    }
    else
    {
        if(!activadasProblemas.isEmpty())
       activadasProblemas.clear();
    }
    dial->setValue(0);

}

void PreguntaItem::changue(int value)
{
    prgres->setValue(value);
    if(value<=25)
    prgres->setFormat("Normal:%p%");
    else
        if(value>25 && value<=50 )
            prgres->setFormat("Bajo:%p%");
    else
            if(value>50 && value<=75 )
                prgres->setFormat("Medio:%p%");
else
                if(value>75 && value<=100 )
                    prgres->setFormat("Alto:%p%");
}
void PreguntaItem::activateRB()
{if(cont==0)
    {
    dial->setEnabled(true);

     labelporcent->setEnabled(true);

     if(minum<14)
     activadasPreguntas.push_back(this);
     else
     activadasProblemas.push_back(this);
    }
cont++;

}

void PreguntaItem::disactivateRB()
{

    dial->setDisabled(true);
    dial->setValue(0);
     labelporcent->setDisabled(true);

     if(minum<14)
     activadasPreguntas.removeOne(this);
     else
     activadasProblemas.removeOne(this);

cont=0;
}


InformativoWidget::InformativoWidget(QString titulo, QString Texto, QImage image, QWidget *parent):
    QWidget(parent)
{

}


sabiasque::sabiasque(QWidget *parent):
    QDialog(parent,Qt::FramelessWindowHint)
{
    a=0;
    this->setWindowTitle("Sabias que?");
    //this->setFixedSize(611, 314);
    QVBoxLayout *vl=new QVBoxLayout;
    sabias= new QWebView(this);
    push=new QPushButton(tr("X"));
    push->setStyleSheet("background:transparent; font-size:22pt; color:red;");
    sabias->load(QUrl(QDir::currentPath()+"/page/index.html"));

    QPalette palette = sabias->palette();
    palette.setBrush(QPalette::Base, Qt::transparent);
    sabias->page()->setPalette(palette);
    sabias->setAttribute(Qt::WA_OpaquePaintEvent, false);




    this->setAttribute(Qt::WA_TranslucentBackground,true);
//this->setWindowOpacity(.92);
    vl->addWidget(push,1,Qt::AlignRight);

vl->addWidget(sabias);
connect(push,SIGNAL(clicked()),this,SLOT(hide()));

this->setLayout(vl);


resize(611, 350);
}

void sabiasque::closeEvent(QCloseEvent *event)
{
    if(a!=1)
    {
   this->hide();
 event->ignore();
    }
}

void sabiasque::resizeEvent(QResizeEvent *)
{

    sabias->resize(this->width(),sabias->height());

}



WigglyWidget::WigglyWidget(QWidget *parent)
    : QWidget(parent)
{
    movie=new QMovie(tr(":/cuc.gif"));

connect(movie,SIGNAL(frameChanged(int)),this,SLOT(paintnewframe(int)));
movie->start();
}

void WigglyWidget::paintnewframe(int)
{
 repaint();
}


void WigglyWidget::paintEvent(QPaintEvent * /* event */)

{
    QPainter painter(this);
    QPixmap pix=movie->currentPixmap();
    painter.drawPixmap(0,0,pix.scaled(this->width()/2,this->height()));
}



getNino::getNino(QWidget *parent):
QWidget(parent)
{
    va=true;
    regi=0;
    vlay=new QVBoxLayout(this);
  tableNino=new QTableWidget(this);
  hlay=new QHBoxLayout(this);

  mapper2=new QSignalMapper(this);
  mapper=new QSignalMapper(this);
  mapper3=new QSignalMapper(this);
  mapper4=new QSignalMapper(this);
  Linebusc=new QLineEdit(this);
  butBusc=new QPushButton(QIcon(":/lupa.png"),tr("Buscar"),this);
  Linebusc->setPlaceholderText(trUtf8("Buscar niño"));
  hlay->addWidget(Linebusc);
  hlay->addWidget(butBusc);
  tableNino->verticalHeader()->setResizeMode(QHeaderView::Fixed);
  tableNino->horizontalHeader()->setResizeMode(QHeaderView::Stretch);


  vlay->addLayout(hlay);
vlay->addWidget(tableNino);
this->setLayout(vlay);
loadNinos();
connect(mapper,SIGNAL(mapped(int)),this,SLOT(mapped(int)));
connect(mapper2,SIGNAL(mapped(int)),this,SLOT(loadescoinfo(int)));
connect(Linebusc,SIGNAL(returnPressed()),this,SLOT(loadNinos()));
connect(butBusc,SIGNAL(clicked()),this,SLOT(loadNinos()));
connect(mapper3,SIGNAL(mapped(int)),this,SLOT(graficar(int)));
connect(mapper4,SIGNAL(mapped(int)),this,SLOT(eliminar(int)));

}

void getNino::loadNinos()
{
    regi=0;
    QString str=Linebusc->text();

    if(va)
    {
        tableNino->clear();
    tableNino->setColumnCount(5);
    tableNino->setHorizontalHeaderItem(0,new QTableWidgetItem(QIcon(":/boy.png"),tr("Nombre")));
    tableNino->setHorizontalHeaderItem(1,new QTableWidgetItem(QIcon(":/age.png"),tr("Edad")));
    tableNino->setHorizontalHeaderItem(2,new QTableWidgetItem(QIcon(":/universidad.png"),tr("Escolaridad")));
    tableNino->setHorizontalHeaderItem(3,new QTableWidgetItem(tr("")));
    tableNino->setHorizontalHeaderItem(4,new QTableWidgetItem(tr("")));



    va=false;
    }
    else
    {

        tableNino->clearContents();

    }
     int total;
    QTableWidgetItem *item;
    int cont=0;
    Conexion::query.exec("select count(1) from Nino where (Nombre like '%"+str+"%')");
    Conexion::query.next();
    total=Conexion::query.value(0).toInt();


        Conexion::query.exec("select Nombre,Edad,Escolaridad,idNino from Nino where (Nombre like '%"+str+"%')");



        tableNino->setRowCount(total);


        while (Conexion::query.next()) {

            for (int i = 0; i < 2; ++i) {
                item=new QTableWidgetItem(Conexion::query.value(i).toString());
                 tableNino->setItem(cont,i,item);
                 item->setFlags(item->flags() & Qt::ItemIsEnabled);

            }

            Esc=new QComboBox(this);
            Esc->addItem( QIcon(tr(":/kinder.png")),tr("Kinder"));
            Esc->addItem( QIcon(tr(":/primaria.png")),tr("Primaria"));
            Esc->addItem( QIcon(tr(":/secundaria.png")),tr("Secundaria"));
            Esc->addItem( QIcon(tr(":/Preparatoria.png")),tr("Preparatoria"));
            Esc->addItem( QIcon(tr(":/universidad.png")),tr("Universidad"));
            Esc->setCurrentIndex(Esc->findText(Conexion::query.value(2).toString()));
            item=new QTableWidgetItem();
            item->setFlags( Qt::ItemIsEnabled);
tableNino->setItem(cont,2,item);
            tableNino->setCellWidget(cont,2,Esc);

            mapper2->setMapping(Esc,cont);

            connect(Esc,SIGNAL(currentIndexChanged(QString)),mapper2,SLOT(map()));


           pushTables=new QPushButton(QIcon(":/lupa.png"),tr("verinfo"),this);
           pushTables->setObjectName("table");
          mapper->setMapping(pushTables,Conexion::query.value(3).toInt());
          connect(pushTables,SIGNAL(clicked()),mapper,SLOT(map()));
          item=new QTableWidgetItem();
            item->setFlags( Qt::ItemIsEnabled);
          tableNino->setItem(cont,3,item);
          tableNino->setCellWidget(cont,3,pushTables);

          pusheliminar=new QPushButton(QIcon(":/eliminar.png"),tr("Eliminar"),this);
          pusheliminar->setObjectName("table");
         mapper4->setMapping(pusheliminar,Conexion::query.value(3).toInt());
         connect(pusheliminar,SIGNAL(clicked()),mapper4,SLOT(map()));
         item=new QTableWidgetItem();
           item->setFlags( Qt::ItemIsEnabled);
         tableNino->setItem(cont,4,item);
         tableNino->setCellWidget(cont,4,pusheliminar);




            cont++;
        }


}


void getNino::mapped(int id)
{
    int count,row=1;
        regi=1;
        ids=id;
    if(!va)
    {
    tableNino->clear();
  tableNino->setColumnCount(7);

  tableNino->setHorizontalHeaderItem(0,new QTableWidgetItem(tr("Hiperactividad")));
  tableNino->setHorizontalHeaderItem(1,new QTableWidgetItem(tr("D.Atencion")));
  tableNino->setHorizontalHeaderItem(2,new QTableWidgetItem(tr("Social")));
  tableNino->setHorizontalHeaderItem(3,new QTableWidgetItem(tr("Cognitiva")));
  tableNino->setHorizontalHeaderItem(4,new QTableWidgetItem(tr("Afectiva")));
  tableNino->setHorizontalHeaderItem(5,new QTableWidgetItem(tr("Fecha")));
  tableNino->setHorizontalHeaderItem(6,new QTableWidgetItem(tr("Ver")));
  va=true;
    }
Conexion::query.exec("update Nino set Edad=cast((select ((julianday('now')-julianday(fecha))/365)+.00001 from Nino where idNino="+QString().setNum(id)+") as integer) where idNino="+QString().setNum(id)+";");
Conexion::query.exec(" select count (1) from Resultados where fk_idNino="+QString().setNum(id)+"");
Conexion::query.next();
count=Conexion::query.value(0).toInt();
Conexion::query.exec("select hiper,atenc,soc,cog,afect,fecha,idres from Resultados where fk_idNino="+QString().setNum(id)+" order by idres desc");
tableNino->setRowCount(count+1);
QTableWidgetItem *it;
while (Conexion::query.next()) {

    for (int i = 0; i < 5; i++) {
        it=new QTableWidgetItem();
        it->setFlags(Qt::ItemIsEnabled);
        tableNino->setItem(row,i,it);
        progres=new QProgressBar(this);
        progres->setRange(0,99);
        //progres->setFormat(text(Conexion::query.value(i).toDouble()));
        progres->setValue(Conexion::query.value(i).toDouble());
        progres->setObjectName("tab");
        tableNino->setCellWidget(row,i,progres);
        buffPr=progres;

    }
    it=new QTableWidgetItem(Conexion::query.value(5).toString());
    it->setFlags(Qt::ItemIsEnabled);
    tableNino->setItem(row,5,it);

    it=new QTableWidgetItem();
    it->setFlags(Qt::ItemIsEnabled);
    tableNino->setItem(row,6,it);
    pushver=new QPushButton(QIcon(":/lupa.png"),tr("Ver"));
     pushver->setObjectName("table");
    tableNino->setCellWidget(row,6,pushver);
    mapper3->setMapping(pushver,Conexion::query.value(6).toInt());
    connect(pushver,SIGNAL(clicked()),mapper3,SLOT(map()));

    row++;
}


}

void getNino::loadescoinfo(int element)
{
  bool introdujo=false;
  int grade;

    QComboBox *combo=qobject_cast<QComboBox *>(tableNino->cellWidget(element,2));
    while (!introdujo) {

    switch (combo->findText(combo->currentText())) {

    case 0:
    case 2:
    case 3 :
        grade=QInputDialog::getInteger(this,tr("Grado"),tr("Introdusca el ultimo grado de escolaridad: "),1,1,3,1,&introdujo);

        break;
    case 1:
        grade=QInputDialog::getInteger(this,tr("Grado"),tr("Introdusca el ultimo grado de escolaridad: "),1,1,6,1,&introdujo);

        break;

    default:
        grade=QInputDialog::getInteger(this,tr("Grado"),tr("Introdusca el ultimo grado de escolaridad: "),1,1,1,1,&introdujo);

        break;
    }
}

    Conexion::query.exec("update Nino set Escolaridad='"+combo->currentText()+"',grado="+QString().setNum(grade)+" where Nombre='"+tableNino->item(element,0)->text()+"'");


}

void getNino::graficar(int id )
{
   Conexion::query.exec("select hiper,atenc,soc,cog,afect,fecha from Resultados where idres="+QString().setNum(id)+"");
   QList<QVariant> list;
   Conexion::query.next();
   list.push_back(Conexion::query.value(2).toDouble());
   list.push_back(Conexion::query.value(3).toDouble());
    list.push_back(Conexion::query.value(4).toDouble());
    list.push_back(Conexion::query.value(0).toDouble());
     list.push_back(Conexion::query.value(1).toDouble());
     list.push_back(Conexion::query.value(5).toString());
    MainWindow *man=new MainWindow(list,parentWidget());

     man->show();

}

QString getNino::text(int value)
{

    if(value<=25)
  return tr("Normal:%p%");
    else
        if(value>25 && value<=50 )
           return tr("Bajo:%p%");
    else
            if(value>50 && value<=75 )
               return tr("Medio:%p%");
else
                if(value>75 && value<=100 )
                    return tr("Alto:%p%");
}

void getNino::closeEvent(QCloseEvent * event)
{
    event->ignore();
}

void getNino::eliminar(int id)
{

      Conexion::query.exec("delete from Resultados where(fk_idNino="+QString().setNum(id)+")");
      Conexion::query.exec("delete from Nino where(idNino="+QString().setNum(id)+")");

    loadNinos();




}

bool getNino::addnew(int w, int h)
{

    if(w==0 && regi==1 )
    {


        regi=0;
         return true;
    }
    else
        return false;
}



Tab::Tab(QWidget *parent):
    QTabWidget(parent)
{

}

void Tab::closeEvent(QCloseEvent *event)
{
    event->ignore();
}
