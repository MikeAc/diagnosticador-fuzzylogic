#ifndef REGISTROALUMNO_H
#define REGISTROALUMNO_H

#include <QDialog>
#include<QtGui>
#include"fuzzyfer.h"
class registroAlumno : public QDialog
{
    Q_OBJECT
public:
    explicit registroAlumno(QWidget *parent = 0);

    Tab *tab;
    getNino *getsNin;
protected:
    void paintEvent(QPaintEvent *event);
   void hideEvent(QHideEvent *);
   void showEvent(QShowEvent *);
signals:

private slots:
     void paintNewFrame(int);
private:


      QMovie *movie;
    QListWidget *contWidget;
    QStackedWidget *pWidget;
    RegistroNinoWidg *regist;
public slots:

    void accion();
    void accion(int,int);
private:
    void createIcon();


};


#endif // REGISTROALUMNO_H
