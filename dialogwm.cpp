#include "dialogwm.h"

dialogWM::dialogWM(QWidget *parent) :
    QDialog(parent,Qt::FramelessWindowHint)
{
    movie=new QMovie(":/unam12.gif");

    connect(movie,SIGNAL(frameChanged(int)),this,SLOT(paintNewFrame(int)));
    movie->start();


}

void dialogWM::destroyme()
{
    this->destroy();
}

void dialogWM::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix=movie->currentPixmap();
    painter.drawPixmap(0,0,pix.scaled(this->size()));
}

void dialogWM::paintNewFrame(int)
{
    this->repaint();
}

