#include "final.h"



    FinalScaleDraw::FinalScaleDraw( Qt::Orientation orientation, const QStringList &labels ):
        d_labels( labels )
    {
        setTickLength( QwtScaleDiv::MinorTick, 0 );
        setTickLength( QwtScaleDiv::MediumTick, 0 );
        setTickLength( QwtScaleDiv::MajorTick, 2 );



        enableComponent( QwtScaleDraw::Backbone, false );

        if ( orientation == Qt::Vertical )
        {
            setLabelRotation( -60.0 );
        }
        else
        {
            setLabelRotation( -20.0 );
        }


        setLabelAlignment(  Qt::AlignVCenter );
    }


   QwtText FinalScaleDraw::label( double value ) const
    {
        QwtText lbl;

        const int index = qRound( value );

        if ( index >= 0 && index <= d_labels.size()  )
        {


            lbl = d_labels[ index ];


            qDebug()<<d_labels[ index ];
        }

        return lbl;
    }


    FinalChartItem::FinalChartItem():
     QwtPlotBarChart( "Page Hits" )
    {
        setLegendMode( QwtPlotBarChart::LegendBarTitles );
        setLegendIconSize( QSize( 10, 14 ) );
    }

    QwtColumnSymbol *FinalChartItem::specialSymbol(int index, const QPointF &) const
    {
        // we want to have individual colors for each bar

        QwtColumnSymbol *symbol = new QwtColumnSymbol( QwtColumnSymbol::Box );
        symbol->setLineWidth( 2 );
        symbol->setFrameStyle( QwtColumnSymbol::Raised );

        QColor c( Qt::white );
        if ( index >= 0 && index < d_colors.size() )
            c = d_colors[ index ];

        symbol->setPalette( c );

        return symbol;
    }

    void FinalChartItem::addDistro( const QString &distro, const QColor &color )
    {
        d_colors += color;
        d_distros += distro;
        itemChanged();
    }


    QwtText FinalChartItem::barTitle( int sampleIndex ) const
    {
        QwtText title;
        if ( sampleIndex >= 0 && sampleIndex < d_distros.size() )
            title = d_distros[ sampleIndex ];

        return title;
    }


BarChart::BarChart(QList<QVariant> list, QWidget *parent ):
    QwtPlot( parent )
{



 const struct
    {
        const char *distro;
        const double hits;
        QColor color;

    } pageHits[] =
    {

     { "Cognitivo", list[1].toDouble(), QColor( 115, 186, 37 ) },
             { "Social", list.at(0).toDouble(), QColor( "LightSkyBlue" ) },
     { "Afectiva", list[2].toDouble(), QColor( "FireBrick" ) },
    {"Hiperactividad",list[3].toDouble(),QColor("yellow")},
     {"Atencion",list[4].toDouble(),QColor("black")}
 };

cont=0;
    setAutoFillBackground( true );
    setPalette( QColor( "Linen" ) );

    QwtPlotCanvas *canvas = new QwtPlotCanvas();
    canvas->setLineWidth( 2 );
    canvas->setFrameStyle( QFrame::Box | QFrame::Sunken );
    canvas->setBorderRadius( 10 );

    QPalette canvasPalette( QColor( "gray" ) );
    canvasPalette.setColor( QPalette::Foreground, QColor( "black" ) );
    canvas->setPalette( canvasPalette );

    setCanvas( canvas );

    setTitle(trUtf8("Resultados de niño en la fecha :").append(list[5].toString()));

    d_barChartItem = new FinalChartItem();

    QVector< double > samples;

    for ( uint i = 0; i < sizeof( pageHits ) / sizeof( pageHits[ 0 ] ); i++ )
    {
        qDebug()<<i;
        d_distros += pageHits[ i ].distro;
        samples += pageHits[ i ].hits;

        d_barChartItem->addDistro(pageHits[ i ].distro, pageHits[ i ].color );
    }

    qDebug()<<samples;
    d_barChartItem->setSamples( samples );

    d_barChartItem->attach( this );

    insertLegend( new QwtLegend );

    setOrientation( 0 );

    setAutoReplot( false );

}

void BarChart::setOrientation( int o )
{
    const Qt::Orientation orientation =
        ( o == 0 ) ? Qt::Vertical : Qt::Horizontal;

    int axis1 = QwtPlot::xBottom;
    int axis2 = QwtPlot::yLeft;

    if ( orientation == Qt::Horizontal )
        qSwap( axis1, axis2 );

    d_barChartItem->setOrientation( orientation );

    setAxisTitle( axis1, "Resultados" );

    QStringList last;
    last.append(" ");
    last.append(" ");
    last.append(" ");
    last.append(" ");
    last.append(" ");
   setAxisScaleDraw( axis1, new FinalScaleDraw( orientation, last ) );


    setAxisTitle( axis2, "Nivel de problema" );
    setAxisScale(axis2,0,100,20);
    setAxisMaxMinor( axis2, 3 );

    QwtScaleDraw *scaleDraw = new QwtScaleDraw();
    scaleDraw->setTickLength( QwtScaleDiv::MediumTick, 2 );
    setAxisScaleDraw( axis2, scaleDraw );

    plotLayout()->setCanvasMargin( 0 );
    replot();

}

void BarChart::exportChart()
{
    QwtPlotRenderer renderer;
    renderer.exportTo( this, QDir::homePath()+"/resultado.pdf" );
}

void BarChart::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QPixmap frame(":/graph.jpg");
       painter.drawPixmap(0,0,frame.scaled(this->size()));
}

MainWindow::MainWindow(QList<QVariant> list, QWidget *parent):
    QMainWindow( parent )
{


    d_chart = new BarChart( list,this );

    setCentralWidget( d_chart );

    QToolBar *toolBar = new QToolBar( this );


    QToolButton *btnExport = new QToolButton( toolBar );
    btnExport->setText( "Export" );
    btnExport->setToolButtonStyle( Qt::ToolButtonTextUnderIcon );

    connect( btnExport, SIGNAL( clicked() ), d_chart, SLOT( exportChart() ) );



    toolBar->addWidget( btnExport );
    addToolBar( toolBar );

}




