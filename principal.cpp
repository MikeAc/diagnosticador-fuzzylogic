#include "principal.h"

Principal::Principal(QWidget *parent) :
    QMainWindow(parent)
{



    QFile File(":p/scroll.qss");
     File.open(QFile::ReadOnly);
     QString StyleShee = QLatin1String(File.readAll());
     File.close();


  lay=new QVBoxLayout;

sabias=new sabiasque();
//this->setAttribute(Qt::WA_DeleteOnClose);
     this->setStyleSheet(StyleShee);

qApp->setApplicationName(trUtf8("Diagnostico de Niños"));
    this->setWindowTitle(trUtf8("Diagnostico de Niños"));
   qApp->setWindowIcon(QIcon(":/boy.png"));


     timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(closerdialog()));
    timer->start(5300);
    verticall=new QVBoxLayout(this);
     QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8"));
   dialog=new dialogWM(this);
    dialog->show();
   l=new registroAlumno(this);
   BoxPreguntas=new QGroupBox(this);
   boxcentral=new QGroupBox(this);
   scrollpr=new QScrollArea(this);
    scrollpr->setStyleSheet("background-color:transparent");
   //scrollpr->setBackgroundRole(QPalette::Dark);
   view=new QWebView();



   view->load(QUrl("qrc:p/index"));

vertical=new QWidgetList;
    split=new QSplitter(Qt::Horizontal,this);

   verticall->setSpacing(4);



   vertical->push_back(new PreguntaItem(trUtf8("\nActua impulsivamente?"),0));

   vertical->push_back(new PreguntaItem(trUtf8("Interrumpe siempre que se le habla"),1));
   vertical->push_back(new PreguntaItem(trUtf8("Es mal perdedor"),2));
   vertical->push_back(new PreguntaItem(trUtf8("Distrae a otros en Clase"),3));
   vertical->push_back(new PreguntaItem(trUtf8("Tiene poco control de sus acciones"),4));
   vertical->push_back(new PreguntaItem(trUtf8("Se frustra facilmente"),5));
   vertical->push_back(new PreguntaItem(trUtf8("Es desobesdiente y no acata indicaciones"),6));
   vertical->push_back(new PreguntaItem(trUtf8("Se pone en riesgo Constantemente"),7));
   vertical->push_back(new PreguntaItem(trUtf8("Hace Rabietas inconctroladas"),8));
   vertical->push_back(new PreguntaItem(trUtf8("No  es empatico con sus compañeros"),9));
   vertical->push_back(new PreguntaItem(trUtf8("No reconoce sus sentimientos"),10));
   vertical->push_back(new PreguntaItem(trUtf8("Muestra desinteres ante las situaciones u eventos"),11));
   vertical->push_back(new PreguntaItem(trUtf8("Es incensible ante el dolor de otros"),12));
   vertical->push_back(new PreguntaItem(trUtf8("Es Desconfiado y hostil"),13));


   vertical->push_back(new PreguntaItem(trUtf8("Tiene Movimiento Constante de pies y manos"),14,Hiperactividad));
   vertical->push_back(new PreguntaItem(trUtf8("Se levanta Constantemente de su lugar"),15,Hiperactividad));
   vertical->push_back(new PreguntaItem(trUtf8("Se muestra activo en situaciones en las que es \ninapropiado hacerlo"),16,Hiperactividad));
   vertical->push_back(new PreguntaItem(trUtf8("Tiene dificultad para jugar con un objetivo"),17,Hiperactividad));
   vertical->push_back(new PreguntaItem(trUtf8("Se muestra intranquilo en actividades de ocio"),18,Hiperactividad));
   vertical->push_back(new PreguntaItem(trUtf8("Se le dificulta esperar un turno"),19,Hiperactividad));
   vertical->push_back(new PreguntaItem(trUtf8("Da las respuestas antes de terminar hacer las preguntas"),20,Hiperactividad));
  vertical->push_back(new PreguntaItem(trUtf8("Interrumpe sus actividades o las de otros"),21,Hiperactividad));
  vertical->push_back(new PreguntaItem(trUtf8("Interrumpe sus juegos o el de sus compañeros."),22,Hiperactividad));
   

   vertical->push_back(new PreguntaItem(trUtf8("No presta atencion a los detalles incurriendo en errores por \ndescuido en cualquier tarea(escolar o cualquier otra)"),23,DeficitAtencion));
   vertical->push_back(new PreguntaItem(trUtf8("Parece no prestar atencion en actividades ludicas"),24,DeficitAtencion));
   vertical->push_back(new PreguntaItem(trUtf8("No parece escuchar cuando se le habla directamente "),25,DeficitAtencion));
   vertical->push_back(new PreguntaItem(trUtf8("Tiene dificultad para realizar sus tareas o actividades"),26,DeficitAtencion));
   vertical->push_back(new PreguntaItem(trUtf8("Se muestra disgustado cuando se le solicita realizar tareas que \nimplican un esfuerzo mental"),27,DeficitAtencion));
   vertical->push_back(new PreguntaItem(trUtf8("No finaliza y no sigue instrucciones en tareas escolares u obigaciones"),28,DeficitAtencion));
   vertical->push_back(new PreguntaItem(trUtf8("Extravía objetos neesarios para sus tareas o actividades"),29,DeficitAtencion));
   vertical->push_back(new PreguntaItem(trUtf8("Es descuidado en las tareras o actividades que realiza"),30,DeficitAtencion));


   vertical->push_back(new PreguntaItem(trUtf8("Intimida a otros"),31,TranstornoSocial));
   vertical->push_back(new PreguntaItem(trUtf8("Inicia Peleas"),32,TranstornoSocial));
   vertical->push_back(new PreguntaItem(trUtf8("Manifiesta crueldad fisica con personas"),33,TranstornoSocial));
   vertical->push_back(new PreguntaItem(trUtf8("Manifiesta crueldad Fisica con animales"),34,TranstornoSocial));
    vertical->push_back(new PreguntaItem(trUtf8("Ha cometido pequeños hurtos"),35,TranstornoSocial));
    vertical->push_back(new PreguntaItem(trUtf8("Ha causado deliberadamente daños a \npersonas o a sus pertenencias"),36,TranstornoSocial));
    vertical->push_back(new PreguntaItem(trUtf8("Frecuentemente Rompe las reglas"),37,TranstornoSocial));
    vertical->push_back(new PreguntaItem(trUtf8("Frecuentemente Molesta a sus compañeros"),38,TranstornoSocial));

    vertical->push_back(new PreguntaItem(trUtf8("Dificultad para Mantener la atencion"),39,DeficitAtencion));

    vertical->push_back(new PreguntaItem(trUtf8("Dificultad para centrarse en una sola tarea o actividad"),40,DeficitAtencion));
    vertical->push_back(new PreguntaItem(trUtf8("No presta suficiente atencion a los detalles"),41,DeficitAtencion));
    vertical->push_back(new PreguntaItem(trUtf8("No sigue instrucciones"),42,DeficitAtencion));
    vertical->push_back(new PreguntaItem(trUtf8("No finaliza tareas o juegos"),43,DeficitAtencion));
    vertical->push_back(new PreguntaItem(trUtf8("Parece no escuchar"),44,DeficitAtencion));
    vertical->push_back(new PreguntaItem(trUtf8("Dificultad para planear, organizar, completar y ejecutar"),45,DeficitAtencion));
    vertical->push_back(new PreguntaItem(trUtf8("Se distrae facilmente"),46,DeficitAtencion));
    vertical->push_back(new PreguntaItem(trUtf8("Amenudo pierde objetos"),47,DeficitAtencion));
animacion();
   // verticall->addWidget(wigl);
    for (int i = 0; i < vertical->size(); ++i) {
        verticall->addWidget(vertical->at(i));

    }

    BoxPreguntas->setLayout(verticall);
   BoxPreguntas->setStyleSheet("QGroupBox{background-color:rgb(230,245,225,80);}");
   BoxPreguntas->setObjectName("group");
  scrollpr->setWidget(BoxPreguntas);
  split->addWidget(scrollpr);
  split->addWidget(view);
  view->hide();
  view->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


  boxcentral->setLayout(lay);

  setCentralWidget(boxcentral);

   this->resize(845, 535);
   createMenubar();

   //lay->setAlignment(wigl,Qt::AlignCenter);
connect(l,SIGNAL(finished(int)),this,SLOT(clo()));



}

void Principal::mostrarPregu(bool )
{

    QWidgetList bufflist;
  scrollpr->takeWidget();
  for (int i = 0; i < vertical->size(); ++i) {
        BoxPreguntas->layout()->removeWidget(vertical->at(i));
 }





BoxPreguntas=new QGroupBox("Cuestionario",this);
   verticall=new QVBoxLayout();




    Fuzzyfer fuzz;
    if(actafect->isChecked() || actcognitiva->isChecked() || actHiper->isChecked() || actsocial->isChecked())
    {
        for (int i = 0; i <14 ; ++i) {
            bufflist.push_back(vertical->at(i));
        }

    }

    if(actHiper->isChecked())
    {
        for (int i = 14; i < 23; ++i) {
            bufflist.push_back(vertical->at(i));
        }
    }


    if(actatencion->isChecked())
    {

        for (int i = 23; i < 31; ++i) {
            bufflist.push_back(vertical->at(i));
        }
        for (int i = 39; i < 48; ++i) {
            bufflist.push_back(vertical->at(i));
        }
    }

    if(actsocial->isChecked())
    {
        for (int i = 31; i < 39; ++i) {
            bufflist.push_back(vertical->at(i));
        }
    }
    //*****

    if(actafect->isChecked()){
    for (int i = 0; i < 14; ++i) {
        for (int j = 0; j < 34; ++j) {
            if(fuzz.matrix[i][j].contains(Afectiva))
            {
                if(!bufflist.contains(vertical->at(j+14))){
                    bufflist.push_back(vertical->at(j+14));
                }
            }
        }
    }
}


    //*********

    if(actcognitiva->isChecked()){
    for (int i = 0; i < 14; ++i) {
        for (int j = 0; j < 34; ++j) {
            if(fuzz.matrix[i][j].contains(Cognitiva))
            {
                if(!bufflist.contains(vertical->at(j+14))){
                    bufflist.push_back(vertical->at(j+14));
                }
            }
        }
    }
}

 //****

    if(actsocial->isChecked()){
    for (int i = 0; i < 14; ++i) {
        for (int j = 0; j < 34; ++j) {
            if(fuzz.matrix[i][j].contains(Social))
            {
                if(!bufflist.contains(vertical->at(j+14))){
                    bufflist.push_back(vertical->at(j+14));
                }
            }
        }
    }
}


    for (int i = 0; i< bufflist.size(); ++i) {
        verticall->addWidget(bufflist.at(i));
    }

    BoxPreguntas->setLayout(verticall);

    scrollpr->setWidget(BoxPreguntas);


    BoxPreguntas->setStyleSheet( "QGroupBox{"
                                 " color:black;"
                                  "font-family:Luxi Sans; "
                                 "font-style:italic;"
                                  "font-size:16pt;"

                                   "border-radius: 6px;"
                          "background-color:rgb(230,245,225,80);}");
    BoxPreguntas->setMinimumSize(150,50);

}

void Principal::createMenubar()
{


    bar=menuBar()->addMenu("Terminar");
    elements=menuBar()->addMenu("Documentacion");
    actseguir=new QAction(QIcon(":/seguir.png"),"Seguir",this);
    connect(actseguir,SIGNAL(triggered()),this,SLOT(seguir()));

    actregresar=new QAction(QIcon(":/atras.png"),"Atras",this);
    connect(actregresar,SIGNAL(triggered()),this,SLOT(atras()));

    actiquitarReco=new QAction(QIcon(":/paloma.png"),"Recomendacion",this);
    actiquitarReco->setCheckable(true);
    connect(actiquitarReco,SIGNAL(triggered(bool)),this,SLOT(elementos(bool)));
    bar->addAction(actseguir);
    bar->addAction(actregresar);
    elements->addAction(actiquitarReco);

    actHiper=new QAction("Hiperactividad",this);
    actHiper->setCheckable(true);
   actHiper->setChecked(true);

    actafect=new QAction("Afectiva",this);
    actafect->setCheckable(true);
actafect->setChecked(true);

    actatencion=new QAction("Atencion",this);
    actatencion->setCheckable(true);
    actatencion->setChecked(true);

    actcognitiva=new QAction("Cognitiva",this);
    actcognitiva->setCheckable(true);
    actcognitiva->setChecked(true);

    actsocial=new QAction("social",this);
    actsocial->setCheckable(true);
    actsocial->setChecked(true);

connect(actHiper,SIGNAL(triggered(bool)),this,SLOT(mostrarPregu(bool)));
connect(actafect,SIGNAL(triggered(bool)),this,SLOT(mostrarPregu(bool)));
connect(actcognitiva,SIGNAL(triggered(bool)),this,SLOT(mostrarPregu(bool)));
connect(actatencion,SIGNAL(triggered(bool)),this,SLOT(mostrarPregu(bool)));
connect(actsocial,SIGNAL(triggered(bool)),this,SLOT(mostrarPregu(bool)));

     problemas=menuBar()->addMenu("Problemas");
problemas->addAction(actHiper);
problemas->addAction(actafect);
problemas->addAction(actatencion);
problemas->addAction(actsocial);
problemas->addAction(actcognitiva);

}

    void Principal::closeEvent(QCloseEvent *)
    {
        l->close();
        sabias->close();
    }

    void Principal::paintEvent(QPaintEvent *event)
    {
        QPainter painter(this);

        QPixmap currentFrame(":/fondo1.png");

            painter.drawPixmap(0,0,currentFrame.scaled(this->size()));
    }


    void Principal::atras()
    {
       l->show();
       this->hide();
    }

    void Principal::seguir()
    {
        QList<QVariant> list;

            if(PreguntaItem::activadasPreguntas.isEmpty() && PreguntaItem::activadasProblemas.isEmpty())
            {
                QMessageBox::critical(this,tr("Error"),tr("Es necesario contestar las preguntas gracias."));
                return;
            }



  Fuzzyfer fuzz;
  list=fuzz.start();
  list.push_back(QDate::currentDate().toString("yyyy-MM-dd"));
  qDebug()<<"insert into Resultados(soc,cog,afect,hiper,atenc,fecha,fk_idNino) values("+list[0].toString()+","+list[1].toString()+","+list[2].toString()+","+list[3].toString()+","+list[4].toString()+",'"+QDate::currentDate().toString("yyyy-MM-dd")+"',"+QString().setNum(getNino::ids)+")";
  Conexion::query.exec("insert into Resultados(soc,cog,afect,hiper,atenc,fecha,fk_idNino) values("+list[0].toString()+","+list[1].toString()+","+list[2].toString()+","+list[3].toString()+","+list[4].toString()+",'"+QDate::currentDate().toString("yyyy-MM-dd")+"',"+QString().setNum(getNino::ids)+")");
  l->show();
l->getsNin->mapped(getNino::ids);

  fin=new MainWindow(list,l->tab);
        fin->show();

        this->hide();


    }

    void Principal::elementos(bool checkar)
    {
        if(checkar)
        {
            view->show();
        }
        else
        {
            view->hide();
        }
    }


    void Principal::closerdialog()
    {

          l->show();
          sabias->show();
          dialog->hide();
         timer->stop();
          timer->disconnect(this);

     dialog->close();

    }





    void Principal::resizeEvent(QResizeEvent *)
    {

        lab->setMaximumWidth((this->width()/3));
        lab->setMinimumWidth((this->width()/3));

    }

    void Principal::clo()
    {
        sabias->a=1;
        sabias->close();
        this->close();
    }

    void Principal::animacion()
    {

QHBoxLayout *hbo= new QHBoxLayout(this);
        wigl=new WigglyWidget(split);

       lab=new QLabel("");
       lab->setMaximumWidth((this->width()/3));
       lab->setMinimumWidth((this->width()/3));
        wigl->setFixedHeight(50);

     hbo->addWidget(lab);
 hbo->addWidget(wigl);



        lay->addLayout(hbo);

      lay->addSpacing(7);
        lay->addWidget(split);

    }
