RC_FILE=img/icons.rc

QT       += core gui\
webkit network\
sql

SOURCES += \
    main.cpp \
    principal.cpp \
    dialogwm.cpp \
    registroalumno.cpp \
    widgets.cpp \
    fuzzyfer.cpp \
    final.cpp \
    conexion.cpp

HEADERS += \
    principal.h \
    dialogwm.h \
    registroalumno.h \
    widgets.h \
    fuzzyfer.h \
    final.h \
    conexion.h

RESOURCES += \
    img/img.qrc \
    pages/pages.qrc \
    pages/page/htmlst.qrc



CONFIG += qwt


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../fuzzylite-5.0/fuzzylite-5.0/fuzzylite/bin/release/ -lfuzzylite-static
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../fuzzylite-5.0/fuzzylite-5.0/fuzzylite/bin/debug/ -lfuzzylite-staticd

INCLUDEPATH += $$PWD/../fuzzylite-5.0/fuzzylite-5.0/fuzzylite
DEPENDPATH += $$PWD/../fuzzylite-5.0/fuzzylite-5.0/fuzzylite
