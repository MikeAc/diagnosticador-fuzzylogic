    #ifndef PRINCIPAL_H
#define PRINCIPAL_H

#include <QMainWindow>
#include<QtGui>
#include"dialogwm.h"
#include"registroalumno.h"
#include<QtWebKit/QWebView>
#include<QUrl>
#include<QThread>



class Principal : public QMainWindow
{
    Q_OBJECT
public:
    explicit Principal(QWidget *parent = 0);


private:
 QLabel *lab;
    WigglyWidget *wigl;
    QLabel *label;
sabiasque *sabias;
QComboBox *eleccion;
     QMovie *movie;
    QScrollArea *scrollpr;
    QTimer *timer;
    dialogWM *dialog;
    registroAlumno *l;
   QWidgetList *vertical;
   QVBoxLayout *verticall;
    QSplitter *split;
    QGroupBox *BoxPreguntas,*boxcentral;
    QWebView *view;
    QMenu *bar,*elements,*problemas;
    void createMenubar();
QAction *actseguir,*actregresar,*actiquitarReco,*actHiper,*actsocial,*actatencion,*actcognitiva,*actafect;
MainWindow *fin;
QVBoxLayout *lay;

protected:
    void closeEvent(QCloseEvent *);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *);



signals:

private  slots:
    void clo();
    void animacion();
    void mostrarPregu(bool);
    void atras();
    void seguir();
    void elementos(bool);
    void closerdialog();

};




#endif // PRINCIPAL_H
