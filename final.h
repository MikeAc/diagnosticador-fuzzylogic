#ifndef FINAL_H
#define FINAL_H

#include <qwt_plot.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_barchart.h>
#include <qwt_column_symbol.h>
#include <qwt_plot_layout.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include<QtGui>
#include<fuzzyfer.h>
class FinalScaleDraw: public QwtScaleDraw
{
public:
    FinalScaleDraw( Qt::Orientation orientation, const QStringList &labels );

    virtual QwtText label( double value ) const;



private:
    QStringList lists;
     QStringList d_labels;
};

class FinalChartItem: public QwtPlotBarChart
{
public:
    FinalChartItem();

  virtual QwtColumnSymbol *specialSymbol(int index, const QPointF& ) const;

    virtual QwtText barTitle( int sampleIndex ) const;
     void addDistro( const QString &distro, const QColor &color );


private:
    QList<QColor> d_colors;
    QList<QString> d_distros;
};




class BarChart: public QwtPlot
{
    Q_OBJECT

public:
    BarChart(QList<QVariant> list,QWidget * = NULL );

public Q_SLOTS:
    void setOrientation( int );
    void exportChart();
protected:
void paintEvent(QPaintEvent *event);

private:
    void populate();
int cont;
    FinalChartItem *d_barChartItem;
    QStringList d_distros;
};

class MainWindow: public QMainWindow
{
public:
    MainWindow(QList<QVariant>,QWidget * = NULL );

protected:

private:
    BarChart *d_chart;
};





#endif // FINAL_H
